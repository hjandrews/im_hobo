.. MET_hobo documentation master file, created by
   sphinx-quickstart on Thu Apr 12 17:16:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================
MET_hobo documentation
======================

Contents:

.. toctree::
   :maxdepth: 2

   includeREADME
   config_file
   modules

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

